# Android-PinView

## Note
Forked from the excellent Github project by chinloong: https://github.com/chinloong/Android-PinView

## Changes
- Converted to Gradle project
- Layout improvements to prevent overlap of textboxes and title in landscape mode
- Refactored core view into separate library and made sample app depend on it

# Original README.md
Android-PinView is a iOS-like Pin Entry/Challenge Activity for Android.
